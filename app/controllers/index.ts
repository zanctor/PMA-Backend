import {Controller, Get} from "routing-controllers";

@Controller("/")
export class IndexController {

    @Get("/")
    get(): string {
        return "Project Management Assistant application";
    }
}