#!/usr/bin/env node
import * as config from "config";
import * as log4js from "log4js";
import app from "../app";
import {Configuration} from "log4js";
import {DatabaseService} from "../services";
import {Container} from "typedi";

let log4jsConfig: Configuration = config.get("log4js");
log4js.configure(log4jsConfig);

const logger = log4js.getLogger("bin/www");
const databaseService = Container.get(DatabaseService);

try {
    databaseService.init()
        .then(() => {
            app.listen(process.env.PORT || 3000);
        })
        .catch((err: Error|string) => {
            logger.error("Application startup error: ", err);
        });
} catch (e) {
    logger.error("Application crashed", e);
    setTimeout(() => process.exit(1), 3000);
}
