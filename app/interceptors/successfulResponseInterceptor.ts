import {Action, Interceptor, InterceptorInterface} from "routing-controllers";

@Interceptor()
export class SuccessfulResponseInterceptor implements InterceptorInterface {

    intercept(action: Action, content: any): any {
        let response: any = {success: true};

        if (content) {
            response.data = content;
        }

        return response;
    }
}