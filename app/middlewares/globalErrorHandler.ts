import {Middleware, ExpressErrorMiddlewareInterface} from "routing-controllers";
import {Request, Response} from "express";

@Middleware({ type: "after" })
export class GlobalErrorHandler implements ExpressErrorMiddlewareInterface {

    error(error: any, request: Request, response: Response, next: (err?: any) => any) {
        let code = error.statusCode || error.httpCode;

        if (!response.headersSent) {
            response.status(code || 500).json({
                success: false,
                error: error.message || error.error || error
            });
        }
    }
}