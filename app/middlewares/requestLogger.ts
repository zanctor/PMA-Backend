import * as log4js from "log4js";
import {Request, Response} from "express";
import {ExpressMiddlewareInterface, Middleware} from "routing-controllers";

const levels = log4js.levels;
const logger = log4js.getLogger("requestLogger");

@Middleware({ type: "before" })
export default class RequestLoggerMiddleware implements ExpressMiddlewareInterface {
    use(req: Request, res: Response, next?: (err?: any) => any): any {
        res.on("finish", () => {
            const data = `${req.method} ${res.statusCode} ${req.originalUrl || req.url}`;
            let level = levels.INFO;

            if (res.statusCode >= 400) {
                level = levels.WARN;
            }

            if (res.statusCode >= 500) {
                level = levels.ERROR;
            }

            logger.log(level, data);
        });
        next();
    }
}