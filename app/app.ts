import "reflect-metadata";
import {Action, createExpressServer, useContainer} from "routing-controllers";
import {Container} from "typedi";
import {Application} from "express";
import {GlobalErrorHandler} from "./middlewares/globalErrorHandler";
import {SuccessfulResponseInterceptor} from "./interceptors/successfulResponseInterceptor";
import RequestLoggerMiddleware from "./middlewares/requestLogger";

useContainer(Container);

const app: Application = createExpressServer({
    defaultErrorHandler: false,
    classTransformer: false,
    controllers: [__dirname + "/controllers/**/*.js"],
    middlewares: [
        RequestLoggerMiddleware,
        GlobalErrorHandler
    ],
    interceptors: [SuccessfulResponseInterceptor],

    authorizationChecker: (action: Action, roles: any) => {
        return null; //TODO
    },
    currentUserChecker: async (action: Action) => {
        return null; //TODO
    }
});

export default app;
