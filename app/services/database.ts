import {Service} from "typedi";
import * as _ from "lodash";
import * as config from "config";
import * as log4js from "log4js";
import {Sequelize} from "sequelize-typescript";
import * as path from "path";
import * as migrate from "node-pg-migrate";

@Service()
export class DatabaseService {
    private sequelize: Sequelize;
    private dbConfig = <any>config.get('db');
    private logger = log4js.getLogger('models');

    public async init(): Promise<void> {
        await this.runMigrations();
        this.initSequelize();

        return this.connect();
    }

    private async runMigrations(): Promise<void> {
        return migrate.default({
            databaseUrl: this.dbConfig.url,
            migrationsTable: "pgmigrations",
            dir: `${process.cwd()}/migrations`,
            count: Infinity,
            ignorePattern: ".",
            direction: "up",
            createMigrationsSchema: true,
            createSchema: true,
            schema: this.dbConfig.schema
        });
    }

    private initSequelize(): void {
        this.sequelize = new Sequelize({
            url: `${this.dbConfig.url}?schema=${this.dbConfig.schema}`,
            modelPaths: [path.join(__dirname, "../models/!(index).*")],
            modelMatch: (filename, member) => {
                return filename === _.camelCase(member);
            },
            logging: (sql: string, time: number) => this.logger.debug(`${sql}  | Execution time: ${time} ms`),
            benchmark: true,
            define: {
                timestamps: false,
                freezeTableName: false,
                underscored: true,
                schema: this.dbConfig.schema
            }
        });
    }

    private async connect(): Promise<void> {
        await this.sequelize.authenticate();
        this.logger.debug('Database connection established successfully');
    }
}
